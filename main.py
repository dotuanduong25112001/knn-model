import cv2
import numpy as np

img1= cv2.imread('digits.png',0)  # đọc ảnh
cells1 = [np.hsplit(row,50) for row in np.vsplit(img1,50)] # chia ảnh thành 50 hàng 50 cột
x=np.array(cells1) # ma trận pixel
train = x[:,:50].reshape(-1,400).astype(np.float32) # trải phẳng mỗi pixel trong block
k=np.arange(10);

train_labels =np.repeat(k,250) [:,np.newaxis] # gán nhãn

# build KNN
knn=cv2.ml.KNearest_create()
knn.train(train,0,train_labels)


# nhận diện ảnh
img= cv2.imread('phone-number.png',0)  # đọc ảnh
cells= [np.hsplit(row,5) for row in np.vsplit(img,1)] # chia ảnh thành block nhỏ

kq=''

# Nhận diện từng block
for x in range (5) :
    cv2.imwrite('anh'+str(x)+'.png',cells[0][x])
    anhnhandien = cv2.imread('anh'+str(x)+'.png', 0)
    x2 = np.array(anhnhandien)
    test = x2.reshape(-1, 400).astype(np.float32)

    kq1, kq2, kq3, kq4 = knn.findNearest(test, 6)  # tìm láng giềng

    kqs=int(kq1)

    kq=kq+str(kqs)
    print (kq1);

print (kq)






